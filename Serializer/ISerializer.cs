﻿
namespace Serializer
{
    public interface ISerializer
    {
        string Serialize<T>(T item);
        T Desirealize<T>(string data) where T : new();
    }
}