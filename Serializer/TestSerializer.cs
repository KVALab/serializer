﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Serializer
{
    public class TestSerializer
    {
        private readonly int iterations;
        private readonly ISerializer serializer;

        public TestSerializer(int iterations, ISerializer serializer)
        {
            this.iterations = iterations;
            this.serializer = serializer;
        }

        public string TestSpeedSerializing<T>(T f) where T : new()
        {
            //var SpeedInfo = (Action<ISerializer, Stopwatch>)((s, w) => { 
            //    Console.WriteLine($"{s.GetType().Name}.{s.GetType().GetMethods().FirstOrDefault().Name} - {w.Elapsed.Seconds}с.{w.Elapsed.Milliseconds}мс.");
            //});

            string outinfo = $"Количество итераций сериализации: {iterations}\r\n";
            Stopwatch stopwatch = new();

            try
            {
                stopwatch.Start();
                string stringProperties = string.Empty;
                for (int i = 0; i < iterations; i++)
                    stringProperties = serializer.Serialize(f);
                stopwatch.Stop();

                outinfo += $"Сериализатор: {serializer.GetType().Name}{Environment.NewLine}Строка свойств класса: {stringProperties}\r\n";
                outinfo += $"{serializer.GetType().Name}.{serializer.GetType().GetMethods().FirstOrDefault().Name}:\t{stopwatch.Elapsed.Seconds}с.{stopwatch.Elapsed.Milliseconds}мс.\r\n";

                stopwatch.Start();
                for (int i = 0; i < iterations; i++)
                {
                    T objF = serializer.Desirealize<T>(stringProperties);
                }
                stopwatch.Stop();

                outinfo += $"{serializer.GetType().Name}.{serializer.GetType().GetMethods().ElementAt(1).Name}:\t{stopwatch.Elapsed.Seconds}с.{stopwatch.Elapsed.Milliseconds}мс.{Environment.NewLine}";
            }
            catch (Exception e)
            {
                throw new FormatException($"Неверный формат данных! \r\n{e.Message}");
            }

            return outinfo;
        }
    }
}
