﻿using System;
using System.IO;
using System.Linq;

namespace Serializer
{
    class Program
    {
        static void Main(string[] args)
        {
            F f = F.GetF();
            
            TestSerializer testSerializer = new TestSerializer(1000000, new CSVSerializer());
            Console.WriteLine(testSerializer.TestSpeedSerializing(f));

            testSerializer = new TestSerializer(1000000, new JsonSerializer());
            Console.WriteLine(testSerializer.TestSpeedSerializing(f));
        }        
    }
}
