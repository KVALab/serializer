﻿using Json = System.Text.Json;

namespace Serializer
{
    public class JsonSerializer : ISerializer
    {
        public string Serialize<T>(T item)
        {
            return Json.JsonSerializer.Serialize<T>(item);
        }

        public T Desirealize<T>(string data) where T : new()
        {
            return Json.JsonSerializer.Deserialize<T>(data);
        }        
    }

}