﻿using System;
using System.Reflection;
using System.Text;

namespace Serializer
{
    public class CSVSerializer : ISerializer
    {
        private readonly StringBuilder stringBuilder = new();        
        
        public string Serialize<T>(T item)
        {
            stringBuilder.Clear();            
            
            var properties = item.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            foreach (var property in properties)
            {                
                stringBuilder.Append($"{property.Name}:{property.GetValue(item)};");                
            }
            return stringBuilder.ToString();
        }

        public T Desirealize<T>(string data) where T: new()
        {
            T t = (T)typeof(T).GetConstructor(Type.EmptyTypes).Invoke(new object[0]);
            
            var properties = t.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            string[] propertiesFromString = data.Split(';', StringSplitOptions.RemoveEmptyEntries);
            if (properties.Length != propertiesFromString.Length)
                throw new FormatException("Количество свойств в объекте не совпадает с переданным количеством!");

            for(int i = 0; i < properties.Length; i++)
            {
                var pair = propertiesFromString[i].Split(':');
                var property = t.GetType().GetProperty(pair[0], BindingFlags.Instance | BindingFlags.Public);
                property.SetValue(t, Convert.ChangeType(pair[1], property.PropertyType));
            }

            return t;
        }
        
    }
}